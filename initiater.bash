#!/bin/bash

WAIT_TIME=$1


if [[ $2 -eq 0 ]]; 

then
	echo "Starting spring-boot reactive"
	mvn -f ./reactive_spring/sample/gateway spring-boot:run &
	PROC_GAT=$(jobs -p | head -n 1)
	echo "*************************************Process Gateway: $PROC_GAT"
	while [[ $COUNTER -lt $3 ]]; do
		mvn -f ./reactive_spring/sample/service spring-boot:run &
		PROC_SER=$(jobs -p | tail -n 1)
		echo "Time: $(date)"
		echo "*************************************Process Service: $PROC_SER" && sleep $WAIT_TIME &
		SLEEP_PAUSE=$!
		wait $SLEEP_PAUSE
		kill $PROC_SER
		echo "Time: $(date)"
		let COUNTER=COUNTER+1
	done;
	kill $PROC_GAT

else
	echo "Starting micronaut"
	#./sample_micronaut/sample/gateway/mvnw -f ./sample_micronaut/sample/gateway mn:run &
	mvn -f ./sample_micronaut/sample/gateway mn:run &
	PROC_GAT=$(jobs -p | head -n 1)
	echo "*************************************Process Gateway: $PROC_GAT"
	while [[ $COUNTER -lt $3 ]]; do
		#./sample_micronaut/sample/gateway/mvnw -f ./sample_micronaut/sample/service mn:run &
		mvn -f ./sample_micronaut/sample/service mn:run &
		PROC_SER=$(jobs -p | tail -n 1)
		echo "Time: $(date)"
		echo "*************************************Process Service: $PROC_SER" && sleep $WAIT_TIME &
		SLEEP_PAUSE=$!
		wait $SLEEP_PAUSE
		kill $PROC_SER
		echo "Time: $(date)"
		let COUNTER=COUNTER+1
	done;
	kill $PROC_GAT
fi
	


